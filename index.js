const axios = require('axios');
const zmq = require('zeromq');
const config = require('dotenv').config()['parsed'];

const API_URL= config['API_URL'];

async function bookStock(id){
  const book = await axios.get(API_URL + '/livres/' + id);

  return book.data.Stock;
};

async function updateBookStock(id, stock){
  await axios.put(API_URL + '/livres/' + id, {
    Stock: stock
  })
}

async function addBook(id){
  const stock = (await bookStock(id)) + 1;
  await updateBookStock(id, stock);
}

async function removeBook(id){
  const stock = (await bookStock(id)) - 1;
  await updateBookStock(id, stock);
}

async function runServer() {
  const sock = new zmq.Reply();
  await sock.bind('tcp://*:' + config['ZEROMQ_PORT']);

  for await (const [result] of sock) {
    const payload = JSON.parse(result.toString());
    console.log(payload);

    if(payload.action == "saveBook"){
      try {
        await removeBook(Number.parseInt(payload.id));
        await sock.send('true');
      } catch (e) {
        console.error(e);
        await sock.send('false');
      }
    }

    if(payload.action == "freeBook"){
      try {
        await addBook(Number.parseInt(payload.id));
        await sock.send('true');
      } catch (e) {
        console.error(e);
        await sock.send('false');
      }
    }
  }
}

runServer();
